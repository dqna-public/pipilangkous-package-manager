# Pipilangkous

!IMPORTANT: Deprecated in favor of GitLab packages

Pipilangkous is a small piece of python code that checks for the `GCPBPIP_KEY` to show the location of a service account file for a specific GCP bucket. If it has access to read it can download files from it and install using the package manager.

## Installation

`pip install git+ssh://git@gitlab.com/dqna/pipilangkous-package-manager.git`

## Usage

### PIP

`pipilangkous pip <package-name>`

`pipilangkous pip3 <package-name>`

### NPM

`pipilangkous npm <package-name>`

## Development

1. Edit the code in `bin/pipilangkous` (IMPORTANT: Not in `pipilangkous.py`)
2. Run the `./build` script.
3. The `pipilangkous` command is available.
