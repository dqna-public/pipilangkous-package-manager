#!/System/Library/Frameworks/Python.framework/Versions/2.7/Resources/Python.app/Contents/MacOS/Python
import os
import click
import warnings
from google.cloud import storage

"""
IMPORTANT: Please only edit the pipilangkous file and not the pipilangkous.py file!
"""


class CommandException(BaseException):
    pass


def download_blob(bucket_name, source_blob_name, destination_file_name):
    service_account = os.getenv("GCPBPIP_KEY")
    if not service_account:
        raise Exception("GCPBPIP_KEY environment variable not found.")
    storage_client = storage.Client.from_service_account_json(
        os.path.expanduser(service_account)
    )
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    try:
        assert blob.exists()
    except Exception as e:
        print(f"Package not found: {source_blob_name}")
        raise e
    blob.download_to_filename(destination_file_name)


@click.group()
def main():
    pass


def command(manager, action, package, bucket_name):
    try:
        package = f"{package}.tar.gz"
        download_blob(bucket_name, package, package)
        os.system(f"{manager} {action} {package}")
        os.remove(package)
    except Exception as e:
        print(e.args[0])


@main.command()
@click.argument("package")
def install(package):
    command("pip3", "install", package, "private_python_modules")
    print("This command is deprecated")


@main.command()
@click.argument("action")
@click.argument("package")
def pip(action, package):
    command("pip", action, package, "private_python_modules")


@main.command()
@click.argument("action")
@click.argument("package")
def pip3(action, package):
    command("pip3", action, package, "private_python_modules")


@main.command()
@click.argument("action")
@click.argument("package")
def npm(action, package):
    command("npm", action, package, "private_javascript_modules")


if __name__ == "__main__":
    main()
