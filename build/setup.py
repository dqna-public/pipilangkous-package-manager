#!/usr/bin/env python3

import setuptools


setuptools.setup(
    name="pipilangkous",
    version="0.0.1",
    packages=setuptools.find_packages(),
    python_requires=">=3.7",
    scripts=["bin/pipilangkous.py", "bin/pipilangkous"],
    install_requires=["click", "google-cloud-storage"],
)
